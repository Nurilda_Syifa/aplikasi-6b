package rasmita.nurilda.app_x06_2c

import android.app.AlertDialog
import android.content.DialogInterface
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.CursorAdapter
import android.widget.ListAdapter
import android.widget.SimpleCursorAdapter
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.frag_data_matkul.*
import kotlinx.android.synthetic.main.frag_data_matkul.view.*
import kotlinx.android.synthetic.main.frag_data_matkul.view.spinner3



class FragmentMatkul : Fragment(), View.OnClickListener, AdapterView.OnItemSelectedListener{
    override fun onNothingSelected(parent: AdapterView<*>?) {
        spinner3.setSelection(0,true)
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        val c = spAdapter.getItem(position) as Cursor
        namaProdi = c.getString(c.getColumnIndex("_id"))
    }
    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnDeleteMatkul->{

            }
            R.id.btnUpdateMatkul->{

            }
            R.id.btnInsertMatkul->{
                dialog.setTitle("Konfirmasi").setIcon(android.R.drawable.ic_dialog_info)
                    .setMessage("Apakah data yang akan dimasukan sudah benar?")
                    .setPositiveButton("Ya",btnInsertDialog)
                    .setNegativeButton("Tidak",null)
                dialog.show()
            }
        }
    }

    lateinit var thisParent : MainActivity
    lateinit var lsAdapter: ListAdapter
    lateinit var spAdapter: SimpleCursorAdapter
    lateinit var dialog : AlertDialog.Builder
    lateinit var  v : View
    var namaMatkul : String=""
    var namaProdi : String=""
    lateinit var db : SQLiteDatabase

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        thisParent = activity as MainActivity
        v = inflater.inflate(R.layout.frag_data_matkul,container,false)
        db = thisParent.getDbObject()
        dialog = AlertDialog.Builder(thisParent)
        v.btnDeleteMatkul.setOnClickListener(this)
        v.btnInsertMatkul.setOnClickListener(this)
        v.btnUpdateMatkul.setOnClickListener(this)
        v.spinner3.onItemSelectedListener = this
        return v
    }


    override fun onStart() {
        super.onStart()
        showDataMatkul("")
        showDataProdi()
    }
    fun showDataMatkul(namaMatkul: String){
        var sql=""
        if(!namaMatkul.trim().equals("")){
            sql = "select mk.kode as _id, mk.nama_matkul, p.nama_prodi from matkul mk, prodi p " +
                    "where mk.id_prodi=p.id_prodi and mk.nama_matkul like '%$namaMatkul%'"
        }else{
            sql = "select mk.kode as _id, mk.nama_matkul, p.nama_prodi from matkul mk, prodi p " +
                    "where mk.id_prodi=p.id_prodi order by mk.nama_matkul asc"
        }
        val c : Cursor = db.rawQuery(sql,null)
        lsAdapter = SimpleCursorAdapter(thisParent,R.layout.item_data_matkul,c,
            arrayOf("_id","nama_matkul","nama_prodi"), intArrayOf(R.id.txKodeMatkul, R.id.txNamaMatkul, R.id.txNProdi),
            CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        v.lsMatkul.adapter = lsAdapter
    }

    fun showDataProdi(){
        val c : Cursor =  db.rawQuery("select nama_prodi as _id from prodi order by nama_prodi asc", null)
        spAdapter = SimpleCursorAdapter(thisParent, android.R.layout.simple_spinner_item,c,
            arrayOf("_id"), intArrayOf(android.R.id.text1),CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        spAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        v.spinner3.adapter = spAdapter
        v.spinner3.setSelection(0)
    }

    fun insertDataMatkul(kode: String, namaMatkul: String, id_prodi: Int){
        var sql = "insert into matkul(kode, nama_matkul, id_prodi) values (?,?,?)"
        db.execSQL(sql, arrayOf(kode,namaMatkul,id_prodi))
        showDataMatkul("")

    }

    val btnInsertDialog = DialogInterface.OnClickListener { dialog, which ->
        var sql =  " select id_prodi from prodi where nama_prodi ='$namaProdi'"
        var c : Cursor =  db.rawQuery(sql, null)
        if(c.count>0){
            c.moveToFirst()
            insertDataMatkul(v.edKodeMatkul.text.toString(), v.edNamaMatkul.text.toString(),
                c.getInt(c.getColumnIndex("id_prodi")))
            v.edKodeMatkul.setText("")
            v.edNamaMatkul.setText("")

        }
    }

}